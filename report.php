<?php
	session_start();
	$servername = "localhost";
	$username = "BusFees";
	$password = "password";
	$dbname = "busfees";

	$conn = mysqli_connect($servername, $username, $password, $dbname);

	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}

	if($_SESSION['rep_date']==null)
	{
		$a="";
	}
	else
	{
		$a=$_SESSION['rep_date'];
	}	

?>


<html>
<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>TMS Report</title>
	</head>
<body>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">TMS Report</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto"></ul>
      <form class="form-inline my-2 my-lg-0">
        <button class="btn btn-outline-success my-2 my-sm-0">Contact Us</button>
      </form>
    </div>
  </nav>


		<div class="container-sm" style="margin-top: 10%;">
		<div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Enter date in YYYY-MM-DD" id="date">
  <div class="input-group-append">
    <button class="btn btn-outline-danger" type="button" id="disp">Button</button>
  </div>
</div>
</div>

<div class="container">
  <h2>Reported Students</h2>
  <p>
  	 <?php
	  	 if($a!='')
	  	 {
	  	 	echo "<strong>"."DATE: "."</strong>".$a;
	  	 } 	  
	 ?>	 
  </p>  
  <button  class="btn btn-danger btn-sm" id="close">CANCEL</button>          
  <table class="table table-striped">
    <thead>
      <tr>
      	<?php 
      		if($a == '' )
			{ 
				echo "<th>".Date."</th>";
			}
      	?>
        <th>Name</th>
        <th>Roll No</th>
        <th>College</th>
        <th>Department</th>
        <th>Area</th>
      </tr>
    </thead>  
    <tbody style="text-transform: uppercase;">
    	 <?php
		    if($_SESSION['user_id']=='admin')
			{
			    
			    if($a!='')
			    {
			    	$sql = " SELECT * FROM `report` WHERE date='$a' ";
			    }	
			    else
			    {
			    	$sql = " SELECT * FROM `report` order by date desc";
			    }			

				$result = mysqli_query($conn, $sql);

				if (mysqli_num_rows($result) >0) 
				{

					while($row = mysqli_fetch_assoc($result)) 
					{
						
						echo "<tr>";	
						 	if($a == '' )
							{ 
								echo "<td>".$row["date"]."</td>";
							}						
					        echo "<td>".$row["name"]."</td>";
					        echo "<td>".$row["rollno"]."</td>";
					        echo "<td>".$row["college"]."</td>";
					        echo "<td>".$row["department"]."</td>";
					        echo "<td>".$row["area"]."</td>";					    
					    echo "</tr>"; 						    					
		    		}			
				} 	
			
			}
		?>           
    </tbody>   
  </table> 
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
				<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script>
    	var input1 = document.getElementById("date");    	
		input1.addEventListener("keyup", function(event) {
		  if (event.keyCode === 13) {
		   event.preventDefault();
		   document.getElementById("disp").click();
		  }
		});
    </script>

	<script>
		$(document).ready(function()
			{
				var date='<?php echo $a ?>';
				if(date=='')
			  	{
			  		document.getElementById("close").style.display="none";
			  	}
			  	else
			  	{
			  		document.getElementById("close").style.display="block";
			  	}
				$('#date').val("<?php echo $a ?>")
				  $("#disp").click(function()
				  {
				  		date=$('#date').val();		                               
		                  $.ajax({
		                        type: "POST",
		                        url: 'selreport.php',
		                        data : {
		                            date:date,
		                        },
		                        success: function(data)
		                        {
		                            if(data==1)
		                            {                                                                
		                                window.location="report.php";
		                            }  	                                             
		                        }
		                   });		                         
					});				  	
					$("#close").click(function()
				  	{
				  		$('#date').val("");
				  		document.getElementById("disp").click();				  						  		
				  	});							
	      })
	</script>

</body>

</html>