-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 10, 2019 at 05:52 PM
-- Server version: 5.7.24-log
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `busfees`
--
CREATE DATABASE IF NOT EXISTS `busfees` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `busfees`;
-- --------------------------------------------------------

--
-- Table structure for table `krce`
--

CREATE TABLE `krce` (
  `sno` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rollno` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `area` varchar(100) NOT NULL,
  `fees` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `krce`
--

INSERT INTO `krce` (`sno`, `name`, `rollno`, `department`, `area`, `fees`) VALUES
(22, 'akdj;lsf', '0987658', 'CSE', 'airport', 'Not Enrolled'),
(19, 'New student', '1234567', 'ECE', 'aaa', 'Enrolled'),
(20, 'kdjs;kah', '7654321', 'Select Department', 'kjahsdkfhka', ''),
(14, 'Assif', 'ecb113', 'ECE', 'Airport', 'Not Enrolled'),
(17, 'asdf', 'ecb113a', 'CSE', 'asd', 'Not Enrolled'),
(18, 'Assif', 'ecb1613', 'ECE', 'airport', 'Not Enrolled'),
(21, 'aksdjl', 'wertyui', 'ECE', 'ajlksdahfdkjla', '');

-- --------------------------------------------------------

--
-- Table structure for table `krct`
--

CREATE TABLE `krct` (
  `sno` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rollno` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `area` varchar(100) NOT NULL,
  `fees` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `krct`
--

INSERT INTO `krct` (`sno`, `name`, `rollno`, `department`, `area`, `fees`) VALUES
(1, 'assif', 'ecb1613', 'ECE', 'trichy', 'Enrolled');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `date` date NOT NULL,
  `name` varchar(100) NOT NULL,
  `rollno` varchar(100) NOT NULL,
  `college` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `area` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`date`, `name`, `rollno`, `college`, `department`, `area`) VALUES
('2019-12-10', 'akdj;lsf', '0987658', 'krce', 'CSE', 'airport'),
('2019-12-10', 'Assif', 'ecb113', 'krce', 'ECE', 'Airport'),
('2019-12-10', 'asdf', 'ecb113a', 'krce', 'CSE', 'asd'),
('2019-12-10', 'Assif', 'ecb1613', 'krce', 'ECE', 'airport');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user`, `password`) VALUES
('admin', 'password'),
('user1', 'pswd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `krce`
--
ALTER TABLE `krce`
  ADD PRIMARY KEY (`rollno`),
  ADD KEY `sno` (`sno`);

--
-- Indexes for table `krct`
--
ALTER TABLE `krct`
  ADD PRIMARY KEY (`rollno`),
  ADD UNIQUE KEY `sno` (`sno`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`rollno`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `krce`
--
ALTER TABLE `krce`
  MODIFY `sno` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `krct`
--
ALTER TABLE `krct`
  MODIFY `sno` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
CREATE USER 'BusFees'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON busfees . * TO 'BusFees'@'localhost';

FLUSH PRIVILEGES;