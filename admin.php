<html>
    <head>
        

        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>TMS Admin</title>
</head>
<body>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Admin - Bus Management System</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto"></ul>
      <form class="form-inline my-2 my-lg-0">
        <button class="btn btn-outline-success my-2 my-sm-0">Contact Us</button>
      </form>
    </div>
  </nav>
  <br><br><br><br><br><br><br><br><br>
  <div class="container-sm">
    <a style="color:white;" type="button" class="btn btn-primary btn-lg btn-block" id="add">Add New Student Details</a>
    <a style="color:white;" type="button" class="btn btn-primary btn-lg btn-block" id="edit">Edit Student Details</a>
     <a style="color:white;" type="button" class="btn btn-primary btn-lg btn-block" id="report">View Report</a>
    <a style="color:white;" type="button" class="btn btn-danger btn-lg btn-block" id="logout">LOGOUT</a>
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
  </body>

    <script>
            $(document).ready(function()
            {
                $("#add").click(function()
                {
                     window.location="add.php";                                		             
                });
                $("#edit").click(function()
                {
                     window.location="edithome.php";                                		             
                });  
                $("#logout").click(function()
                {
                     window.location="logout.php";                                		             
                }); 
                 $("#report").click(function()
                {
                     window.location="report.php";                                                 
                });   
            });
            
    </script>
</html>

