<html>
    <head>
        
        <title>Buss Fees</title>

        <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">New Student details Upload</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto"></ul>
      <form class="form-inline my-2 my-lg-0">
        <button class="btn btn-outline-success my-2 my-sm-0">Contact Us</button>
      </form>
    </div>
  </nav>
  <div class="container-sm">
    <div class="form-group">
      <label for="exampleInputEmail1">Name</label>
      <input type="text" class="form-control" id="name" aria-describedby="emailHelp" style="text-transform: uppercase;">
      <span id="er1" style="color:red"></span>
    </div>
    <div class="form-group">
     <label for="exampleInputEmail1">Roll Number</label>
     <input type="text" class="form-control" id="rno" aria-describedby="emailHelp" style="text-transform: uppercase;">
      <span id="er2" style="color:red"></span>
   </div>
   <div class="form-group">
     <label for="exampleInputEmail1">College</label>
     <select class="form-control form-control" id="clge">
     <option value="undef">Select College</option>
     <option value="krce">KRCE</option>
     <option value="krct" >KRCT</option>
   </select>
   <span id="er3" style="color:red"></span>
   </div>
   <div class="form-group">
     <label for="exampleInputEmail1">Department</label>
     <select class="form-control form-control" id="dppt">
        <option value="undef">Select Department</option>
        <option value="ECE">ECE</option>
        <option value="CSE">CSE</option>
        <option value="EEE">EEE</option>
        <option value="MECH">MECH</option>
        <option value="CIVIL">CIVIL</option>
    </select>
    <span id="er4" style="color:red"></span>
   </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Area</label>
    <input type="text" class="form-control" id="area" aria-describedby="emailHelp" style="text-transform: uppercase;">
    <span id="er5" style="color:red"></span>
  </div>






  <fieldset class="form-group">
    <div class="row">
      <legend class="col-form-label col-sm-2 pt-0">STATUS :</legend>
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gridRadios"  value="Enrolled" id="enr">
          <label class="form-check-label" for="gridRadios1">
            Enrolled
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gridRadios" value="Not Enrolled" id="nenr">
          <label class="form-check-label" for="gridRadios2">
            Not-Enrolled
          </label>
         </div>
         <span id="er6" style="color:red"></span>
      </div>      
    </div>
  </fieldset>




    <button class="btn btn-primary btn-sm" id="upload">SUBMIT</button>  
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    

        <script type="text/javascript">            
            $(document).ready(function()
            {	
                var a,b,c,d,e,f;
                $("#enr").click(function()
                {                      
                         f=$('#enr').val(); 
                })
                $("#nenr").click(function()
                {                      
                         f=$('#nenr').val(); 
                })	               
                $("#upload").click(function()
                {			  		
                     a=$('#name').val();	
                     b=$('#rno').val();	
                     c=$('#clge').val();	
                     d=$('#dppt').val();	
                     e=$('#area').val();
                     if(a.length>=4)
                        $('#er1').html("");
                      if(b.length==7)
                        $('#er2').html("");
                      if(c!="undef")
                        $('#er3').html("");
                      if(d!="undef")
                        $('#er4').html("");
                      if(e.length>=3)
                        $('#er5').html("");
                      if(f!=undefined)
                        $('#er6').html("");
                     if(a.length>=4 && b.length==7 && c!="undef" && d!="undef" && e.length>=3 && f != undefined )
                     {                                                                                         
                        $.ajax({
                                type: "POST",
                                url: 'update.php',
                                data : {
                                    a:a,
                                    b:b,
                                    c:c,
                                    d:d,
                                    e:e,
                                    f:f,
                                },
                                success: function(data)
                                {
                                    if(data==1)
                                    {                                
                                        alert("Updated Success");
                                        window.location = "admin.php";
                                    }  
                                    else
                                    {
                                        alert("Someting Went Wrong");
                                    }	                                              
                                }
                            });		
                    }
                    else
                    {
                      if(!(a.length>=4))
                        $('#er1').html("Atleast 4 Characters Needed");
                      if(!(b.length==7))
                        $('#er2').html("Invalid Roll Number");
                      if(c=="undef")
                        $('#er3').html("Select College");
                      if(d=="undef")
                        $('#er4').html("Select Department");
                      if(!(e.length>=3))
                        $('#er5').html("Atleast 3 Characters Needed");
                      if(f==undefined)
                        $('#er6').html("Select Department");
                    }

                });  
            });
        </script>
    </body>
</html>