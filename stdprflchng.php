<?php
session_start();
$servername = "localhost";
$username = "BusFees";
$password = "password";
$dbname = "busfees";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$b=$_SESSION['std_rno'];

if($_SESSION['std_clg'] == 'krce' and $_SESSION['user_id']=='admin'){

	$sql = " SELECT * FROM `krce` WHERE `rollno` LIKE '$b' ";

	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) == 1) {
		$row = mysqli_fetch_assoc($result);
		$a = $row["name"];
		$c = $_SESSION['std_clg'];
		$d = $row["department"];
		$e = $row["area"];
		$f = $row["fees"];
	} 	

}

if($_SESSION['std_clg'] == 'krct' and $_SESSION['user_id']=='admin'){

	$sql = " SELECT * FROM `krct` WHERE `rollno` LIKE '$b' ";

	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) == 1) {
		$row = mysqli_fetch_assoc($result);
		$a = $row["name"];
		$c = $_SESSION['std_clg'];
		$d = $row["department"];
		$e = $row["area"];
		$f = $row["fees"];
	} 	

}
?>

<html>

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <title>Data Edit</title>
	</head>

	<body>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Edit Student details</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto"></ul>
      <form class="form-inline my-2 my-lg-0">
        <button class="btn btn-outline-success my-2 my-sm-0">Contact Us</button>
      </form>
    </div>
  </nav>
  <div class="container-sm">
   <form>
    <div class="form-group">
      <label for="exampleInputEmail1">Name</label>
      <input type="text" class="form-control" value="<?php echo "$a" ?>"  style="text-transform: uppercase;" disabled>
    </div>
    <div class="form-group">
     <label for="exampleInputEmail1">Roll Number</label>
     <input type="text" class="form-control"  value="<?php echo "$b" ?>" style="text-transform: uppercase;"  disabled>
   </div>
   <div class="form-group">
     <label for="exampleInputEmail1">College</label>
     <input type="text" class="form-control"  value="<?php echo "$c" ?>"  style="text-transform: uppercase;" disabled>
   </div>
			<div class="form-group">
     <label for="exampleInputEmail1">Department</label>
     <input type="text" class="form-control"  value="<?php echo "$d" ?>"  style="text-transform: uppercase;" disabled>
   </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Area</label>
    <input type="text" class="form-control"  value="<?php echo "$e" ?>"  style="text-transform: uppercase;" disabled>
  </div>
		<div class="form-group">
     <label for="exampleInputEmail1">STATUS</label>
     <select class="form-control form-control" id="fees" >
							<option value="Enrolled">Enrolled</option>
							<option value="Not Enrolled">Not Enrolled</option>
					</select>
   </div>
		<a class="btn btn-primary btn-sm" id="update">SUBMIT</a>
  </form>
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->    
  </body>    

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   
    <script>
    $(document).ready(function()
    {
      $('#fees').val("<?php echo "$f" ?>"); 
      $("#update").click(function()
            {             
                    var b="<?php echo "$b" ?>"
                    var c="<?php echo "$c" ?>"
                    var f=$('#fees').val();                          
                    $.ajax({
                            type: "POST",
                            url: 'stdprflchngdb.php',
                            data : {
                                b:b,
                                c:c,
                                f:f,
                            },
                            success: function(data)
                            {
                                if(data==1)
                                {                                
                                    alert("Updated Success");
                                }  
                                else
                                {
                                    alert("Someting Went Wrong");
                                }                                               
                            }
                        });                                                              
                });   
    });
  </script>

</html>