<html>
<head>
        
        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>TMS Edit</title>
    </head>
    <body>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Edit User Details</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto"></ul>
      <form class="form-inline my-2 my-lg-0">
        <button class="btn btn-outline-success my-2 my-sm-0">Contact Us</button>
      </form>
    </div>
  </nav>
  <br><br><br><br><br><br><br><br><br>
  <div class="container-sm">
   
    <div class="form-group">
     <label for="exampleInputEmail1">College</label>
     <select class="form-control form-control" id="clg">
     <option>Select College</option>
     <option value="krce">KRCE</option>
     <option value="krct">KRCT</option>
   </select>
   </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Roll Number</label>
      <input type="text" class="form-control" id="rno" style="text-transform: uppercase;">
    </div>
    <button  class="btn btn-primary btn-sm" id="edit">GET DETAILS</button>
    <button  class="btn btn-primary btn-sm" id="logout">LOG OUT</button>
  
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
    <script>
      var input1 = document.getElementById("rno");
      input1.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
         event.preventDefault();
         document.getElementById("edit").click();
        }
      });
    </script>
    <script type="text/javascript">
		$(document).ready(function()
		{
			  $("#edit").click(function()
			  {
			  		var rno=$('#rno').val();
                    var clg=$('#clg').val();		                               
	                  $.ajax({
	                        type: "POST",
	                        url: 'stdidset.php',
	                        data : {
	                            rno:rno,
                                clg:clg,
	                        },
	                        success: function(data)
	                        {
	                            if(data==1)
	                            {                                                                
	                                window.location="profile.php";
	                            }  	                                             
	                        }
	                    });		              		             
						});
						$("#logout").click(function()
                {
                     window.location="logout.php";                                		             
                });  
      })
    </script>
				</body>
</html>